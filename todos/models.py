from django.db import models

# # Create your models here.
# The TodoList model
# Your TodoList model must have the following features.

# A name property that contains up to 100 characters of text
# A created_on property that contains the date and time of when it was created and should default to that using the auto_now_add feature
# A __str__ method that returns the value of the TodoList's name property
# To run the tests just for the TodoList model, use the following command.

# python manage.py test tests.todo_lists.test_model
# The TodoItem model
# Your TodoItem model must have the following features.

# A task property that contains up to 100 characters of text
# A due_date property that contains an optional date and time of when the item should be completed
# An is_completed property that indicates true, the item is completed, or false, it is not
# A list property that is a foreign key relationship to the TodoList that creates a 
# related collection named "items" and will automatically delete the todo items if the todo list is 
# deleted (this is a cascade)
# A __str__ method that returns the value of the TodoItems task property

class TodoList (models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.name)



class TodoItem (models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey("TodoList",related_name="items", on_delete=models.CASCADE,)

    def __str__(self):
        return str(self.task)



