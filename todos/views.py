#from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from todos.models import TodoItem, TodoList

class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
  #  success_url = reverse_lazy("recipes_list")


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    # Redirects to the detail page for the
    # instance just created, assuming that
    # the path name is registered
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    # Redirects to the detail page for the
    # instance just created, assuming that
    # the path name is registered
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


# class TodoListCreateView(CreateView):
#     model = TodoItem
#     template_name = "todos/create.html"
#     fields = ["task", "is_completed", "list"]

#     # Redirects to the detail page for the
#     # instance just created, assuming that
#     # the path name is registered
#     def get_success_url(self):
#         return reverse_lazy("todos_detail", args=[self.object.id])



class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    # Redirects to the detail page for the
    # instance just created, assuming that
    # the path name is registered
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    # Redirects to the detail page for the
    # instance just created, assuming that
    # the path name is registered
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"

    # Redirects to the list view for the model
    # assuming that the path name is registered
    success_url = reverse_lazy("todos_list")

class TodoItemDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/items/delete.html"

    # Redirects to the list view for the model
    # assuming that the path name is registered
    success_url = reverse_lazy("todos_list")