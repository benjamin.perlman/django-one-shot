from django.urls import path

from todos.views import (

TodoItemCreateView, TodoItemDeleteView, TodoItemUpdateView, TodoListDeleteView, TodoListUpdateView, TodoListView, TodoListDetailView, TodoListCreateView

)
urlpatterns = [
    path("", TodoListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_detail"),
    path("create/", TodoListCreateView.as_view(), name="todo_create"),
    path("items/create/", TodoItemCreateView.as_view(), name="todo_item_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todo_item_edit"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todo_edit"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_delete"),
    path("items/<int:pk>/delete/", TodoItemDeleteView.as_view(), name="todo_item_delete"),
]



    # path("", RecipeListView.as_view(), name="recipes_list"),
    # path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    # path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    # path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    # path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    # path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
  